from django.urls import path
from mysite import views

app_name='mysite'

urlpatterns = [
    path('signupdata',views.signupdata,name='signupdata'),
    path('check_user',views.check_user,name='check_user'),
    path('get_item',views.get_item,name='get_item'),
    path('contact/',views.contact,name='contact'),
    path('cart/',views.add_cart,name='cart'),
    path('uslogin/',views.uslogin,name='uslogin'),
    path('uslogout/',views.uslogout,name='uslogout'),
    path('removecart/',views.removeCart,name='removecart'),
    path('grandTotal/',views.grandTotal,name='grandTotal'),
    path('checkout/',views.checkout,name='checkout'),
    path('get_order/',views.get_order,name='get_order'),
    path('success_payment/',views.success_payment,name='success_payment'),
    path('order_history/',views.order_history,name='order_history'),
    path('restaurant/',views.restaurant,name='restaurant'),
    path('viewmenu/',views.viewmenu,name='viewmenu'),
    path('allitems/',views.allitems,name='allitems'),
    path('search_cat/',views.search_cat,name='search_cat'),

    path('profile/',views.profile, name='profile'),
    path('change_profile/',views.change_profile, name='change_profile'),
    path('singleorder/',views.order_history, name='orders'),
    path('change_password/',views.change_password, name='change_password'),
    path('about/',views.about, name='about'),


    path('dsignup/',views.dsignup, name='dsignup'),
    path('delivery_partner/',views.deliverypartner, name='delivery_partner'),
    path('changestatus/',views.changestatus, name='changestatus'),
    path('dprofile/',views.dprofile, name='dprofile'),

]